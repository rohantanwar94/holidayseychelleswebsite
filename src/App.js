import React from 'react'
import './App.css';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Home from './Screen/Home';
import HotelListing from './Screen/HotelListing';
import HotelDetails from "./Component/HotelDetails/HotelDetails";
import { Route, HashRouter as Router, Switch } from 'react-router-dom'
function App() {
  return (

    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/hotellisting" component={HotelListing} />
        <Route exact path="/hotelDetails" component={HotelDetails}></Route>
      </Switch>
    </Router>

  );
}

export default App;
