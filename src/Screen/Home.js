import React from 'react'
import Header from '../Component/Header';
import Banner from '../Component/Banner';
import Superoffers from '../Component/Superoffers';
import FixedNav from "../Component/Navbar/Navbar";
import Event from "../Component/Events/Event";
import Footer from "../Component/Footer";
import NewHeader from "../Component/NewHeader/NewHeader";

export default function Home() {
    return (
        <div>
            <NewHeader/>
            <FixedNav />
            <Banner/>
            <Superoffers/>
            <Event />
            <Footer />
        </div>
    )
}
