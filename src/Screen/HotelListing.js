/* eslint-disable */

import React, { Component } from 'react';
import Header from '../Component/Header';
import './HotelListing.css';
import SmallTiles from "../Component/SmallCategoryTiles/smallTiles";
import CategoryTiles from "../Component/CategoryTiles/categoryTiles";
import Footer from "../Component/Footer";
import { DateRangePicker } from 'react-dates';
import { MoviesData, renderMovieTitle } from '../Component/axios';
import Autocomplete from 'react-autocomplete';
import Autosuggest from 'react-autosuggest';
import HotelList from "../Component/HostelList/HotelList";
import NewHeader from "../Component/NewHeader/NewHeader";



import HotelImage1 from "../assets/images/hotel/01.jpg";
import HotelImage2 from "../assets/images/hotel/02.jpg";
import HotelImage3 from "../assets/images/hotel/03.jpg";
import HotelImage4 from "../assets/images/hotel/hotel4.jpg";


import IslandImage1 from "../assets/images/top-destinations/Island.jpg";
import IslandImage2 from "../assets/images/top-destinations/Mahe-island.jpg";
import IslandImage3 from "../assets/images/top-destinations/praslinisland.jpg";
import IslandImage4 from "../assets/images/top-destinations/Silhouette.jpg";


import BeachImage1 from "../assets/images/trips/mahebeach01.jpg";
import BeachImage2 from "../assets/images/trips/mahebeach02.jpg";
import BeachImage3 from "../assets/images/trips/mahebeach03.jpg";
import BeachImage4 from "../assets/images/trips/mahebeach04.jpg";


import AdventureImage1 from "../assets/images/deals-packages/adventurehimalaya.jpg";
import AdventureImage2 from "../assets/images/deals-packages/LadakhSuper.jpg";
import AdventureImage3 from "../assets/images/deals-packages/Monastery.jpg";
import AdventureImage4 from "../assets/images/deals-packages/Nubra.jpg";


const hotelImages = {
    image1: HotelImage1,
    image2: HotelImage2,
    image3: HotelImage3,
    image4: HotelImage4
}

const hotelImages1 = {
    image1: HotelImage4,
    image2: HotelImage2,
    image3: HotelImage3,
    image4: HotelImage1
}

const hotelImages2 = {
    image1: HotelImage3,
    image2: HotelImage1,
    image3: HotelImage2,
    image4: HotelImage4
}

const islandImages = {
    image1: IslandImage1,
    image2: IslandImage2,
    image3: IslandImage3,
    image4: IslandImage4
}

const beachImages = {
    image1: BeachImage1,
    image2: BeachImage2,
    image3: BeachImage3,
    image4: BeachImage4
}

const adventureImages = {
    image1: AdventureImage1,
    image2: AdventureImage2,
    image3: AdventureImage3,
    image4: AdventureImage4
}

export default class HotelListing extends Component {

    constructor(props) {
        super(props);
        this.state = {
            startDate: null,
            endDate: null,
            isOpen: false,
            adultCounter: 0,
            childCounter: 0,
            roomCounter: 1,
            dropdownVisible: false,
            selected: 'Accommodations',
            searchInput: '',
            itemsZone: [],
            error: null,
            val: '',
            value: '',
            suggestions: [],
            showResults: false,
            defaultZoneval: 'Seychelles'
        };
    }
    setSelectedTabOptions = (tab) => {
        this.setState({ selected: tab });
    }

    setDatefunction(startDate, endDate) {

        // const startD = moment(startDate).format("MMM Do YY");
        // const endD = moment(endDate).format("MMM Do YY");
        this.setState({ startDate, endDate })
    }
    toggleOpen = () => this.setState({ isOpen: !this.state.isOpen });


    increment = () => {
        if (this.state.adultCounter < 15) {
            this.setState({ adultCounter: this.state.adultCounter + 1 });
        }
    }

    decrement = () => {
        if (this.state.adultCounter > 0) {
            this.setState({ adultCounter: this.state.adultCounter - 1 });
        }
    }

    incrementChild = () => {
        if (this.state.adultCounter == 0) {
            this.setState({ adultCounter: this.state.adultCounter + 1 });
        }
        if (this.state.childCounter < 6) {
            this.setState({ childCounter: this.state.childCounter + 1 });
        }

    }

    decrementChild = () => {
        if (this.state.childCounter > 0) {
            this.setState({ childCounter: this.state.childCounter - 1 });
        }
    }

    incrementRoom = () => {
        if (this.state.roomCounter < 10) {
            this.setState({ roomCounter: this.state.roomCounter + 1 });
        }
    }

    decrementRoom = () => {
        if (this.state.roomCounter > 0) {
            this.setState({ roomCounter: this.state.roomCounter - 1 });
        }
    }
    componentDidMount() {
        MoviesData().then(
            (data) => {
                this.setState({

                    itemsZone: data,
                });
            },
            (error) => {
                this.setState({
                    error
                });
            }
        );
    }


    componentWillUnmount() {
        // document.removeEventListener('click', this.globalClickListener)
        window.removeEventListener('click', this.globalClickListener)
    }
    globalClickListener = (nativeEvent) => {

        this.setState({ dropdownVisible: false, showResults: false }, () => {
            console.log('global click')
            console.log(this.state.showResults)
            // document.removeEventListener('click', this.globalClickListener)
            window.removeEventListener('click', this.globalClickListener)
        })
    }
    toggleDropdown = (syntheticEvent) => {
        console.log('toggle dropdown')
        syntheticEvent.stopPropagation();
        this.setState(prevState => ({ dropdownVisible: !prevState.dropdownVisible }), () => {
            if (this.state.dropdownVisible) {
                console.log(this.state.dropdownVisible);

                // document.addEventListener('click', this.globalClickListener)
                window.addEventListener('click', this.globalClickListener)
            }
        })
        // console.log(this.state.dropdownVisible);
    }
    handleBodyClick = (event) => {
        console.log('body click')
        event.stopPropagation()
    }

    renderDropdownMenu() {
        return (
            <div className="panel-dropdown-content" onClick={this.handleBodyClick}>

                <div className="qtyButtons">

                    <label>Adults</label>
                    <button className="qtyDec1" onClick={this.decrement}>
                        &mdash;</button>
                    <span>{this.state.adultCounter}</span>
                    <button className="qtyInc1" onClick={this.increment}>
                        &#xff0b;</button>
                </div>
                <div className="qtyButtons">
                    <label>Childrens</label>
                    <button className="qtyDec1" onClick={this.decrementChild}>
                        &mdash;</button>
                    <span>{this.state.childCounter}</span>
                    <button className="qtyInc1" onClick={this.incrementChild}>
                        &#xff0b;</button>
                </div>
                <div className="qtyButtons">
                    <label>Rooms</label>
                    <button className="qtyDec1" onClick={this.decrementRoom}>
                        &mdash;</button>
                    <span>{this.state.roomCounter}</span>
                    <button className="qtyInc1" onClick={this.incrementRoom}>
                        &#xff0b;</button>
                </div>

            </div>
        )
    }



    getSuggestions = (value) => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? [] : this.state.itemsZone.filter(lang =>
            lang.name.toLowerCase().slice(0, inputLength) === inputValue
        );
    };

    // When suggestion is clicked, Autosuggest needs to populate the input
    // based on the clicked suggestion. Teach Autosuggest how to calculate the
    // input value for every given suggestion.
    getSuggestionValue = (suggestion) => suggestion.name;

    // Use your imagination to render suggestions.
    renderSuggestion = (suggestion) => (
        <div className="font14 darkText  noChild  clickable"><div className="spaceBetween makeFlex hrtlCenter "><div className="flexOne"><p className="locusLabel appendBottom5">{suggestion.name}</p></div><span className="width60 font14 lightGreyText latoBold appendBottom5 appendLeft10 textRight" style={{ width: '130px' }}>{suggestion.id}</span></div></div>
    );
    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue,
            defaultZoneval: newValue,
        });
    };

    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.
    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };

    // Autosuggest will call this function every time you need to clear suggestions.
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    onSuggestionSelected = () => {
        this.setState({
            showResults: false
        });
    }

    showZonelistdiv = (syntheticEvent) => {

        syntheticEvent.stopPropagation();
        this.setState(prevState => ({ showResults: !prevState.showResults }), () => {
            if (this.state.showResults) {
                console.log(this.state.showResults);

                // document.addEventListener('click', this.globalClickListener)
                window.addEventListener('click', this.globalClickListener)
            }
        })
    }

    render() {
        const menuClass = `panel-dropdown${this.state.dropdownVisible ? " active" : ""}`;
        const { value, suggestions } = this.state;

        // Autosuggest will pass through all these props to the input.
        const inputProps = {
            placeholder: 'Enter city/ Hotel/ Area/ Building',
            type: "search",
            value,
            onChange: this.onChange
        };
        return (
            <React.Fragment>
                <div>
                    <NewHeader />
                    <div className="_Hlisting_header">
                        <div className="hsw">
                            <div className="hsw_inner">
                                <div className="hsw_inputBox  " onClick={this.showZonelistdiv}>
                                    <label for="city" className="lbl_input latoBold font12 blueText">CITY, AREA or PROPERTY</label>
                                    <p className="hsw_inputField font16 whiteText" style={{ marginBottom: '3px' }}>{this.state.defaultZoneval}</p>
                                    <div className="focused_div">
                                        {this.state.showResults ?
                                            <div className="autocomplete-wrapper" onClick={this.handleBodyClick}>
                                                <Autosuggest
                                                    suggestions={suggestions}
                                                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                                    onSuggestionSelected={this.onSuggestionSelected}
                                                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                                    getSuggestionValue={this.getSuggestionValue}
                                                    renderSuggestion={this.renderSuggestion}
                                                    inputProps={inputProps}
                                                />
                                            </div> : null
                                        }

                                    </div>
                                </div>
                                <div className="hsw_inputBox  " >
                                    <label for="checkin" className="lbl_input latoBold font12 blueText capText">Check-In:</label>
                                    <input type="text" className="hsw_inputField font16 whiteText" readonly="" value="Tue, 8 Jun 2021" />
                                    {/* <DateRangePicker
                                                        startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                                        startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                                                        endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                                        endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                                                        onDatesChange={({ startDate, endDate }) => this.setDatefunction(startDate, endDate)} // PropTypes.func.isRequired,
                                                        focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                                        onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                                                    /> */}
                                </div>
                                <div className="hsw_inputBox   "
                                ><label for="checkout" className="lbl_input latoBold font12 blueText capText">Check-Out:</label>
                                    <input id="checkout" type="text" className="hsw_inputField font16 whiteText" readonly="" value="Mon, 14 Jun 2021" />

                                </div>
                                <div className="hsw_inputBox  " onClick={this.toggleDropdown}>
                                    <label for="guest" className="lbl_input latoBold font12 blueText">ROOMS &amp; GUESTS</label>

                                    <div className={menuClass} >
                                        {
                                            this.state.dropdownVisible &&
                                            this.renderDropdownMenu()
                                        }
                                    </div>
                                    <p className="hsw_inputField guests font16 whiteText" style={{ marginBottom: '0px' }}>{this.state.roomCounter}<span> Room</span>  {this.state.adultCounter + this.state.childCounter}<span> Guests</span></p>
                                </div><button className="primaryBtn hsw_searchButton " >Search</button></div></div>
                    </div>

                    <div className="_Hlisting_header_viewMap">
                        <div className="hsw_viewMap">
                        <div className="hsw_viewMap_content"><span style={{fontSize:'16px'}}><b>|</b> Showing 3706 properties  in Seychelles</span></div>
                        
                            <div class="miniMapCont"><img src="https://imgak.mmtcdn.com/pwa_v3/pwa_hotel_assets/web/map-mini.png" alt="" /></div>
                        </div>
                    </div>
                    <div className="container makeFlex spaceBetween">
                        <div className="filterWrap appendRight30">
                            <div className="appendBottom35">
                                <p className="makeFlex spaceBetween end"><span className="latoBold font20 blackText appendBottom10">Select Filters</span></p>

                                <div className="filterRow" id="hlistpg_fr_star_category">
                                    <div className="latoBold font16 blackText appendBottom15 makeFlex hrtlCenter">Property Type</div>
                                    <ul className="filterList">
                                        <li>
                                            <label>
                                                <input type="checkbox" className="icheck" />
                                                <span className="cat_star">Apartment</span> <small className="star_score">(2225)</small> </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="checkbox" className="icheck" />
                                                <span className="cat_star">Hotel</span> <small className="star_score">(1226)</small> </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="checkbox" className="icheck" />
                                                <span className="cat_star">Villa</span> <small className="star_score">(225)</small> </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="checkbox" className="icheck" />
                                                <span className="cat_star">Hostel</span> <small className="star_score">(128)</small> </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="checkbox" className="icheck" />
                                                <span className="cat_star">BnB</span> <small className="star_score">(12)</small> </label>
                                        </li>
                                    </ul></div>

                            </div>

                            <div className="filterRow" id="hlistpg_fr_star_category">
                                <div className="latoBold font16 blackText appendBottom15 makeFlex hrtlCenter">Facility</div>
                                <ul className="filterList">
                                    <li>
                                        <label>
                                            <input type="checkbox" className="icheck" />
                                            <span className="cat_star">Railway Station Transfers</span> <small className="star_score">(25)</small> </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" className="icheck" />
                                            <span className="cat_star">Room Service</span> <small className="star_score">(262)</small> </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" className="icheck" />
                                            <span className="cat_star">Wi-Fi</span> <small className="star_score">(2522)</small> </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" className="icheck" />
                                            <span className="cat_star">Living Room</span> <small className="star_score">(1800)</small> </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" className="icheck" />
                                            <span className="cat_star">Airport Transfers</span> <small className="star_score">(1222)</small> </label>
                                    </li>
                                </ul></div>

                            <div className="filterRow" id="hlistpg_fr_star_category">
                                <div className="latoBold font16 blackText appendBottom15 makeFlex hrtlCenter">Star Category</div>
                                <ul className="filterList">
                                    <li>
                                        <label>
                                            <input type="checkbox" className="icheck" />
                                            <span className="cat_star"><i className="icon_star"></i><i className="icon_star"></i><i className="icon_star"></i><i className="icon_star"></i><i className="icon_star"></i></span> <small className="star_score">(25)</small> </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" className="icheck" />
                                            <span className="cat_star"><i className="icon_star"></i><i className="icon_star"></i><i className="icon_star"></i><i className="icon_star"></i></span> <small className="star_score">(26)</small> </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" className="icheck" />
                                            <span className="cat_star"><i className="icon_star"></i><i className="icon_star"></i><i className="icon_star"></i></span> <small className="star_score">(25)</small> </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" className="icheck" />
                                            <span className="cat_star"><i className="icon_star"></i><i className="icon_star"></i></span> <small className="star_score">(18)</small> </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" className="icheck" />
                                            <span className="cat_star"><i className="icon_star"></i></span> <small className="star_score">(12)</small> </label>
                                    </li>
                                </ul></div>
                        </div>
                        <div className="listingWrap">
                            <div className="container container-custom margin_30_95">
                                {/* <SmallTiles /> */}
                                <HotelList Image={hotelImages}  />
                                <HotelList Image={hotelImages1} />
                                <HotelList Image={hotelImages2} />
                                <HotelList Image={hotelImages} />
                                <HotelList Image={hotelImages} />
                                <HotelList Image={hotelImages} />

                            </div>
                        </div>

                    </div>

                </div>
            </React.Fragment>
        )
    }
}
