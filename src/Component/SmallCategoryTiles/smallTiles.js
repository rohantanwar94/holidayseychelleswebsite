import React from 'react';
import HotelImage from "../../assets/images/hotel/01.jpg";
import ApartmentImage from "../../assets/images/island/apartments.jpg";
import VillaImage from "../../assets/images/island/villa.jpg";
import ResortImage from "../../assets/images/island/Resorts.jpg";
import GuestHouseImage from "../../assets/images/island/GuestHouse.jpg";
import LiveAboardImage from "../../assets/images/island/LiveAboard.jpg";




import "./smallTiles.css";

export default function smallTiles(props) {
     return (
          <div className="categories-container">
               <h3>Discover Pure Comfort With all Types of Accommodations</h3>
               <div className="all-tiles">

                    <div className="tile">
                         <a href="#">
                              <img src={HotelImage} />
                              <p>Hotels</p>
                         </a>
                    </div>


                    <div className="tile">
                         <a href="#">
                              <img src={ApartmentImage} />
                              <p>Apartments</p>
                         </a>
                    </div>


                    <div className="tile">
                         <a href="#">
                              <img src={VillaImage} />
                              <p>Villas</p>
                         </a>
                    </div>


                    <div className="tile">
                         <a href="#">
                              <img src={ResortImage} />
                              <p>Resorts</p>
                         </a>
                    </div>

                    <div className="tile">
                         <a href="#">
                              <img src={GuestHouseImage} />
                              <p>GuestHouse</p>
                         </a>

                    </div>

                    <div className="tile">
                         <a href="#" >
                              <img src={LiveAboardImage} />
                              <p>LiveAboard</p>
                         </a>
                    </div>
               </div>
          </div>
     )
}
