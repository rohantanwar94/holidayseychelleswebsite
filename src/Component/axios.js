import React from 'react'

export async function MoviesData() {
  try {
    let question = await fetch(
        "tour/getzonelist",
        {
            method: "GET", //Request Type
            headers: {
                //Header Defination
                "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
                Authorization: "Basic YWRtaW46MTIzNA==",
                "x-api-key": "neeraj@123",
            },
        }
    );
    let result = await question.json();
    if (result.data.length > 1) {
        return result.data;
    }

} catch (error) {
    throw error;
}
    }

export function renderMovieTitle(state, val) {
    return (
        state.name.toLowerCase().indexOf(val.toLowerCase()) !== -1
    );
}