import React, { useState } from 'react';
import "./FixedPriceTile.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart, faShare, faPlus } from '@fortawesome/free-solid-svg-icons';
import DropDownButtons from "../DropDownButtons/dropDownButton";
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css';

function FixedPriceTile(props) {

    const [date, changeDate] = useState(null);
    const [dateOut,changeDateOut]=useState(null);
    const [classToggler,changeClass]=useState("fixed-price-container")
        window.onscroll=()=>{
            console.log(document.body.scrollTop);
                    if (document.body.scrollTop > 4000 || document.documentElement.scrollTop > 4000) {
                        console.log("price");
                        changeClass("price-container");
                    } 
                    else {
                        console.log("fixed");
                        changeClass("fixed-price-container");
                    }
                }
    return (
        <div className={classToggler}>
            <div className="SubContainer">
                <div className="fixed-price-header">
                    <div><h1 style={{ display: "inline-block" }}>€659</h1><br /><span>per night</span></div>
                    <div style={{ marginLeft: "40%" }}>
                        <i>Good</i><p>350 reviews</p>
                    </div>
                    <div className="small-rating-tile">
                        <h4 style={{ color: "white" }}>4.0</h4>
                    </div>
                </div>
                <hr />
                <div className="price-inputs">
                    <form>
                        <DropDownButtons guestClass={props.guestClass}
                            adultIncrement={props.adultIncrement}
                            adultDecrement={props.adultDecrement}
                            childIncrement={props.childIncrement}
                            childDecrement={props.childDecrement}
                            valueGuest={props.valueGuest}
                            valueAdult={props.valueAdult}
                            valueChildren={props.valueChildren}
                        />
                        <div className="date">
                            <label for="">Check In:</label>
                            <DatePicker
                                value={date}
                                selected={date}
                                onChange={d => changeDate(d)}
                                dateFormat="dd/MM/yyyy"
                                minDate={new Date()}></DatePicker>
                        </div>
                        <div className="date">
                            <label for="">Check Out:</label>
                            <DatePicker
                                value={dateOut}
                                selected={dateOut}
                                onChange={d => changeDateOut(d)}
                                dateFormat="dd/MM/yyyy"
                                minDate={new Date()}></DatePicker>
                        </div>
                        <input type="text" style={{marginTop:"10px"}} id="Guests" onClick={props.guestHandler} value={"Guests " + props.valueGuest} />
                        <button className="check">Check Availabilty</button>
                        <button className="payment">Proceed to Payment</button>

                    </form>
                </div>
            </div>
            <div className="btns">
                <button className="shareBtn"><FontAwesomeIcon icon={faShare}></FontAwesomeIcon>Share</button>
                <button className="SaveBtn"><FontAwesomeIcon icon={faHeart}></FontAwesomeIcon>Save</button>
                <button className="AddBtn"><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>Add to Trip</button>
            </div>
        </div>
    )
}

export default FixedPriceTile
