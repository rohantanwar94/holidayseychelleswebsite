import React,{useState} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import "./dropDownButtons.css";

function DropDownButton(props) {

    // const [classToggler,changeClass]=useState(props.guestClass);
    
    // // window.onscroll=()=>{
    // //     console.log(document.body.scrollTop);
    // //             if (document.body.scrollTop > 4000 || document.documentElement.scrollTop > 4000) {
    // //                 if(props.guestClass === "buttons")
    // //                 {
    // //                      changeClass("buttons-fixed");   
    // //                 }
    // //             } 
    // //             else {
    // //                 changeClass(props.guestClass);
    // //             }
    // //         }

    return (
        <div className= {props.guestClass}>
            <div className="Adults">
                <p>Adult</p>
                <div style={{width:"60%", display:"flex",justifyContent:'space-evenly'}}>
                    <button onClick={props.adultDecrement}><FontAwesomeIcon icon={faMinus} /></button>
                    <span>{props.valueAdult}</span>
                    <button onClick={props.adultIncrement}><FontAwesomeIcon icon={faPlus}  /></button>
                </div>
            </div>
            <div className="children">
                <p>children</p>
                <div style={{width:"60%", display:"flex",justifyContent:'space-evenly'}}>
                    <button onClick={props.childDecrement} ><FontAwesomeIcon icon={faMinus}  /></button>
                    <span>{props.valueChildren}</span>
                    <button onClick={props.childIncrement}><FontAwesomeIcon icon={faPlus}  /></button>
                </div>
            </div>
        </div>
    )
}

export default DropDownButton;
