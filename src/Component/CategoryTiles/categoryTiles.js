import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import icon from "../../assets/images/favicon.ico";




import "./categoryTiles.css";

export default function categoryTiles(props) {
     return (
          <div className="new-categories-container">
               <h3>{props.text}</h3>
               <h5 className="subTitle" style={{color:'black'}}>Everyday is a journey and your journey starts here</h5>
               <div className="new-all-tiles">

                    <figure className="new-tile">
                         <a href="#">
                             <img className="icon" src={icon} />
                              <img src={props.images.image1} />
                              <p><FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon><br/>
                              €659/day
                              </p>
                              <p className="hotelDetails" style={{color:'black'}}>
                                  <span style={{fontWeight:'bold'}}>Name</span><br/>
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                              </p></a>
                    </figure>


                    <figure className="new-tile">
                         <a href="#">
                         <img className="icon" src={icon} />
                              <img src={props.images.image2} />
                              
                              <p><FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon><br/>
                              €659/day
                              </p>
                              <p className="hotelDetails" style={{color:'black'}}>
                                  <span style={{fontWeight:'bold'}}>Name</span><br/>
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></a>
                    </figure>


                    <figure className="new-tile">
                         <a href="#">
                         <img className="icon" src={icon} />
                              <img src={props.images.image3} />
                              <p><FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon><br/>
                              €659/day
                              </p>
                              <p className="hotelDetails" style={{color:'black'}}>
                                  <span style={{fontWeight:'bold'}}>Name</span><br/>
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                         </a>
                    </figure>


                    <figure className="new-tile">
                         <a href="#">
                         <img className="icon" src={icon} />
                              <img src={props.images.image4} />
                              <p><FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                              <FontAwesomeIcon icon={faStar}></FontAwesomeIcon><br/>
                              €659/day
                              </p>
                              <p className="hotelDetails" style={{color:'black'}}>
                                  <span style={{fontWeight:'bold'}}>Name</span><br/>
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                         </a>
                    </figure>

               </div>
               <a href="#"><p className="viewAll" style={{marginTop:"200px"}}>View All (357) {'>'}</p></a>
          </div>
     )
}
