import React from 'react';
import "./Event.css";
import EventImage1 from "../../assets/images/events/ChildrenCamp.jpg";
import EventImage2 from "../../assets/images/events/CreoleFestival.jpg";
import EventImage3 from "../../assets/images/events/FashionWeek.jpg";
import EventImage4 from "../../assets/images/events/NatureTrail.jpg";
import EventImage5 from "../../assets/images/events/Saturday.jpg";
import EventImage6 from "../../assets/images/events/TourismFestival.jpg";
import EventImage7 from "../../assets/images/events/Tournament.jpg";
import EventImage8 from "../../assets/images/events/Trail.jpg";


function Event() {
    return (
        <div className="main-container">
            <div className="text">
                <h2>Events</h2>
                <p>Everyday is a journey and your journey starts here</p>
                <hr />
            </div>
            <div className="new-buttons">
                <a href="#">
                <button className="btn1">
                    Today
                </button>
                </a>
                <a href="#">
                <button className="btn1">
                    Tommorow
                </button>
                </a>
                <a href="#">
                <button className="btn1">
                    This Weekend
                </button>
                </a>
                <a href="#">
                <button className="btn1">
                    Next Week
                </button>
                </a>
                <a href="#">
                <button className="btn1">
                    Next Month
                </button>
                </a>
                <a href="#">
                <button className="btn1">
                    More
                </button>
                </a>
            </div>
            <div className="Event-tiles">
                <div className="Event-tile">
                    <img src={EventImage1} />
                    <div className="text-top">
                        <h4>Art for children camp</h4>
                        <p>8 December,2021</p>
                    </div>
                </div>
                <div className="Event-tile">
                    <img src={EventImage2} />
                    <div className="text-top">
                        <h4>Creole Festival</h4>
                        <p>8 December,2021</p>
                    </div>
                </div>
                <div className="Event-tile">
                    <img src={EventImage3} />
                    <div className="text-top">
                        <h4>Seychelles Fshion Week</h4>
                        <p>8 December,2021-15 December 2021</p>
                    </div>
                </div>
                <div className="Event-tile">
                    <img src={EventImage4} />
                    <div className="text-top">
                        <h4>Seychelles Adventure touring</h4>
                        <p>8 December,2021</p>
                    </div>
                </div>
                <div className="Event-tile">
                    <img src={EventImage5} />
                    <div className="text-top">
                        <h4>Live Band</h4>
                        <p>8 December,2021</p>
                    </div>
                </div>
                <div className="Event-tile">
                    <img src={EventImage6} />
                    <div className="text-top">
                        <h4>Tourism Festival</h4>
                        <p>8 December,2021</p>
                    </div>
                </div>
                <div className="Event-tile">
                    <img src={EventImage7} />
                    <div className="text-top">
                        <h4>Seychelles nature Trail</h4>
                        <p>8 December,2021</p>
                    </div>
                </div>
                <div className="Event-tile">
                    <img src={EventImage8} />
                    <div className="text-top">
                        <h4>Art for children camp</h4>
                        <p>8 December,2021</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Event;
