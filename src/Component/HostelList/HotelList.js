import React, { useState } from 'react';
import "./HotelList.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar, faChild, faGlassMartiniAlt, faWifi } from '@fortawesome/free-solid-svg-icons';
import Logo from "../../assets/images/logo/logo.png";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";


function HotelList(props) {

    const [ImageChanger, setImageChanger] = useState(props.Image.image1);

    const onMouseOutFun = (image) => {
        setImageChanger(image);
    }
    const onMouseOverFun = (image) => {
        setImageChanger(image);
    }
    return (
        <div className="Container-Tile">

            <Link to="/hotelDetails">
                <a href="#">
                    <div className="Image-Section">
                        <img src={ImageChanger} />
                        <div className="SmallImage">
                            <img className="imgThumb" src={props.Image.image1}
                                onMouseOver={e => onMouseOverFun(e.currentTarget.src)}
                                onMouseOut={e => onMouseOutFun(e.currentTarget.src)} />
                            <img className="imgThumb" src={props.Image.image2}
                                onMouseOver={e => onMouseOverFun(e.currentTarget.src)}
                                onMouseOut={e => onMouseOutFun(e.currentTarget.src)}
                            />
                            <img className="imgThumb" src={props.Image.image3}
                                onMouseOver={e => onMouseOverFun(e.currentTarget.src)}
                                onMouseOut={e => onMouseOutFun(e.currentTarget.src)}
                            />
                            <img className="imgThumb" src={props.Image.image4}
                                onMouseOver={e => onMouseOverFun(e.currentTarget.src)}
                                onMouseOut={e => onMouseOutFun(e.currentTarget.src)}
                            />
                        </div>
                    </div>
                    <div className="Text-Section">
                        <div className="Text-Contents">
                            <img src={Logo} className="logo" />
                            <div className="Text-Ratings">
                                <h3>Hotel Name</h3>
                                <div>
                                    <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                                    <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                                    <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                                    <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
                                </div>
                            </div>
                            <div className="Small-Text">
                                <p className="address" style={{ color: 'gray' }}>Random Address</p>
                                <p>Near some Building</p>
                                <p className="highlight">Couple Friendly</p>
                                <div className="Extras">
                                    <p style={{ marginLeft: "10px" }}><FontAwesomeIcon icon={faChild}></FontAwesomeIcon>  Kids Play Area</p>
                                    <p style={{ marginLeft: "10px" }}><FontAwesomeIcon icon={faWifi}></FontAwesomeIcon>  Free Wifi</p>
                                    <p style={{ marginLeft: "10px" }}><FontAwesomeIcon icon={faGlassMartiniAlt}></FontAwesomeIcon>  Bar</p>
                                </div>

                                <p style={{ color: 'green' }}><span className="options">More Options:</span>BreakFast Included</p>
                                <p style={{ color: 'red' }}>Great Choice! Booked 200+ times in last 15 days</p>
                            </div>
                        </div>
                        <div className="Price">
                            <h4 style={{ color: 'red', fontWeight: 'bold' }}><del>₹4,899</del></h4>
                            <h3 style={{ color: 'black', fontWeight: 'bold' }}>₹4,000</h3>
                            <p className="sms">Login and Unlock a Secret Deal</p>
                        </div>
                    </div>
                </a>
            </Link>
        </div>

    )
}

export default HotelList;
