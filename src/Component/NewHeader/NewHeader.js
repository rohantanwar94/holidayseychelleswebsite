import React from 'react';
import "./NewHeader.css";
import Logo from "../../assets/images/logo/logo-02.png";
import Icon from "../../assets/images/favicon.ico";
import { BrowserRouter, Link, Switch, Route,Redirect } from "react-router-dom";

function NewHeader() {
    return (
        <>
            <div className="Movable-Container">
                <div className="left-logo" style={{width:'130px', marginLeft:'15px'}}>
                    {/* <Redirect to="/" /> */}
                    <img src={Logo} style={{ cursor: "pointer" }} />
                </div>
                <div className="right">
                    <div style={{ display: "flex" }}>
                        My Trips
                <img src={Icon} />
                    </div>
                    <div>Offers</div>
                    <div>Sign In</div>
                </div>
            </div>
        </>
    )
}

export default NewHeader;
