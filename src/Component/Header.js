import React from 'react'
import './Header.css';

function Header() {
    return (
        <div id="page" style={{backgroundColor:"yellow"}}>
                        <header className="header menu_fixed">
                <div className="chHeaderContainer">
                    {/* <div id="preloader">
                        <div data-loader="circle-side"></div>
                    </div> */}

                    <div id="logo"> <a href="index.php" className="chMmtLogo"> <img src="images/logo/logo.png" alt="" className="logo_normal" /> <img
                        src="images/logo/logo-02.png" alt="" className="logo_sticky" /> </a> </div>
                    <ul id="top_menu">
                        <li><a href="#sign-in-dialog" id="sign-in" className="login" title="Sign In">Sign In</a></li>
                    </ul>
                    {/* <nav className="main-menu2">
                    <ul style={{display:'flex'}}>
                        <li style={{width:'50px'}}><a className="makeFlex hrtlCenter column" href=""><img src="images/background-images/icon01.png" alt="" className="header_img"/><span>Accommodations</span></a>
                            <div className="linew"></div>
                        </li>
                        <li style={{width:'50px'}}><a href=""><img src="images/background-images/02.png" alt="" className="header_img"/><span>Attractions &amp; Things to Do</span></a></li>
                        <li style={{width:'50px'}}><a href=""><img src="images/background-images/03.png" alt="" className="header_img"/><span>Packages</span></a></li>
                        <li style={{width:'50px'}}><a href=""><img src="images/background-images/icon04.png" alt="" className="header_img"/><span>Land &amp; Ferry Transfers</span></a></li>

                        <li style={{width:'50px'}}><a href=""><img src="images/background-images/icon06.png" alt="" className="header_img"/><span>Liveaboard</span></a></li>
                    </ul>
                </nav> */}

                    <a href="#menu" className="btn_mobile">
                        <div className="hamburger hamburger--spin" id="hamburger">
                            <div className="hamburger-box">
                                <div className="hamburger-inner"></div>
                            </div>
                        </div>
                    </a>
                    <nav id="menu" className="main-menu">
                        <ul>
                            <li><span><a href="#" className="wishlist_bt_top">My Trips <img src="images/icon/menuico.png"
                            /></a></span></li>
                            <li><span><a href="#">Offers</a></span></li>
                        </ul>
                    </nav>
                </div>
            </header>
         
            </div>
    )
}

export default Header;