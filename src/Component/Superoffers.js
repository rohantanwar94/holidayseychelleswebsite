import React, { Component } from "react";
import Slider from "react-slick";

export default class Responsive extends Component {
  render() {
    var settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    return (
        <div>
        <div className="BestPackages">
        <div className="container container-custom margin_80_0 AndPackages">
            <div className="main_title_2">
                <h2>Best Deals And Packages</h2>
                <p>Everyday is a journey and your journey starts here</p>
                <span><em></em></span>
            </div>
        <Slider {...settings}>
        <div className="item">
                    <div className="box_grid">
                        <figure> <a href="#0" className=""></a> <a href="#"><img src="images/trips/ScenicMaheIsland.jpg"
                                    className="img-fluid" alt="" width="800" height="533" /></a> </figure>
                        <div className="wrapper">
                            <h3><a href="#">Scenic Mahe Island</a></h3>
                            <p>Suitable for couple & Family |Package can be customized as per each Family Requirement
                            </p>
                            <span className="price"><strong>€659 </strong> /per person</span>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <div className="box_grid">
                        <figure> <a href="#0" className=""></a> <a href="#"><img src="images/trips/BestMahe.jpg"
                                    className="img-fluid" alt="" width="800" height="533"/></a> </figure>
                        <div className="wrapper">
                            <h3><a href="#">Best of Mahe Island</a></h3>
                            <p>Suitable for couple & Family |Package can be customized as per each Family Requirement
                            </p>
                            <span className="price"><strong>€659 </strong> /per person</span>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <div className="box_grid">
                        <figure> <a href="#0" className=""></a> <a href="#"><img src="images/trips/Experience.jpg"
                                    className="img-fluid" alt="" width="800" height="533"/></a> </figure>
                        <div className="wrapper">
                            <h3><a href="#">Experience Praslin</a></h3>
                            <p>Suitable for couple & Family |Package can be customized as per each Family Requirement
                            </p>
                            <span className="price"><strong>€659 </strong> /per person</span>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <div className="box_grid">
                        <figure> <a href="#0" className=""></a> <a href="#"><img src="images/trips/ShortStay.jpg"
                                    className="img-fluid" alt="" width="800" height="533"/></a> </figure>
                        <div className="wrapper">
                            <h3><a href="#">Short Stay Mahe </a></h3>
                            <p>Suitable for couple & Family |Package can be customized as per each Family Requirement
                            </p>
                            <span className="price"><strong>€659 </strong> /per person</span>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <div className="box_grid">
                        <figure> <a href="#0" className=""></a> <a href="#"><img src="images/trips/Mesmerizing.jpg"
                                    className="img-fluid" alt="" width="800" height="533"/></a> </figure>
                        <div className="wrapper">
                            <h3><a href="#">Mesmerizing Mahe</a></h3>
                            <p>Suitable for couple & Family |Package can be customized as per each Family Requirement
                            </p>
                            <span className="price"><strong>€659 </strong> /per person</span>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <div className="box_grid">
                        <figure> <a href="#0" className=""></a> <a href="#"><img src="images/trips/ScenicMaheIsland.jpg"
                                    className="img-fluid" alt="" width="800" height="533" /></a> </figure>
                        <div className="wrapper">
                            <h3><a href="#">Scenic Mahe Island</a></h3>
                            <p>Suitable for couple & Family |Package can be customized as per each Family Requirement
                            </p>
                            <span className="price"><strong>€659 </strong> /per person</span>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <div className="box_grid">
                        <figure> <a href="#0" className=""></a> <a href="#"><img src="images/trips/BestMahe.jpg"
                                    className="img-fluid" alt="" width="800" height="533"/></a> </figure>
                        <div className="wrapper">
                            <h3><a href="#">Best of Mahe Island</a></h3>
                            <p>Suitable for couple & Family |Package can be customized as per each Family Requirement
                            </p>
                            <span className="price"><strong>€659 </strong> /per person</span>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <div className="box_grid">
                        <figure> <a href="#0" className=""></a> <a href="#"><img src="images/trips/Experience.jpg"
                                    className="img-fluid" alt="" width="800" height="533"/></a> </figure>
                        <div className="wrapper">
                            <h3><a href="#">Experience Praslin</a></h3>
                            <p>Suitable for couple & Family |Package can be customized as per each Family Requirement
                            </p>
                            <span className="price"><strong>€659 </strong> /per person</span>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <div className="box_grid">
                        <figure> <a href="#0" className=""></a> <a href="#"><img src="images/trips/ShortStay.jpg"
                                    className="img-fluid" alt="" width="800" height="533"/></a> </figure>
                        <div className="wrapper">
                            <h3><a href="#">Short Stay Mahe </a></h3>
                            <p>Suitable for couple & Family |Package can be customized as per each Family Requirement
                            </p>
                            <span className="price"><strong>€659 </strong> /per person</span>
                        </div>
                    </div>
                </div>
                <div className="item">
                    <div className="box_grid">
                        <figure> <a href="#0" className=""></a> <a href="#"><img src="images/trips/Mesmerizing.jpg"
                                    className="img-fluid" alt="" width="800" height="533"/></a> </figure>
                        <div className="wrapper">
                            <h3><a href="#">Mesmerizing Mahe</a></h3>
                            <p>Suitable for couple & Family |Package can be customized as per each Family Requirement
                            </p>
                            <span className="price"><strong>€659 </strong> /per person</span>
                        </div>
                    </div>
                </div>
        </Slider>
      </div>
      </div>
      <div className="call_section">
        <div className="container clearfix">
            <div className="row">
                <div className="col-md-3"></div>
                <div className="col-lg-6 col-md-6 center wow animated" data-wow-offset="250" style={{visibility: 'visible'}}>
                    <div className="block-reveal">
                        <div className="box_1"> <img src="images/icon/shipping.png" alt="" className="img-responsive"/>
                            <h3>Build Your Own Trip</h3>
                            <p>Find And Create Fully Customized Day By Day Itinerary Of Places To Stay And Things To Do
                                During Your Seychelles Travels. You Can Add Your Interest By Choosing From Our Defined
                                Itinerary Or Create Your Own.</p>
                            <center>
                                <a href="plan-you-own-itenrary.php" className="btn_1 rounded">Start Planning</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      </div>
    );
  }
}