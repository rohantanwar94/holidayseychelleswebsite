/* eslint-disable */

import React, { Component } from 'react';
import './Banner.css';
import moment from 'moment';
import { DateRangePicker } from 'react-dates';
import { MoviesData, renderMovieTitle } from './axios';
import Autocomplete from 'react-autocomplete';
import Autosuggest from 'react-autosuggest';
import BannerTabNav from './BannerTabNav';
import BannerTab from './BannerTab';
import {Link} from 'react-router-dom';
class Banner extends Component {

    constructor(props) {
        super(props);
        this.state = {
            startDate: null,
            endDate: null,
            isOpen: false,
            adultCounter: 0,
            childCounter: 0,
            roomCounter: 1,
            dropdownVisible: false,
            selected: 'Accommodations',
            searchInput: '',
            itemsZone: [],
            error: null,
            val: '',
            value: '',
            suggestions: [],
            showResults: false,
            defaultZoneval: 'Seychelles'
        };
    }
    setSelectedTabOptions = (tab) => {
        this.setState({ selected: tab });
    }

    setDatefunction(startDate, endDate) {

        // const startD = moment(startDate).format("MMM Do YY");
        // const endD = moment(endDate).format("MMM Do YY");
        this.setState({ startDate, endDate })
    }
    toggleOpen = () => this.setState({ isOpen: !this.state.isOpen });


    increment = () => {
        if (this.state.adultCounter < 15) {
            this.setState({ adultCounter: this.state.adultCounter + 1 });
        }
    }

    decrement = () => {
        if (this.state.adultCounter > 0) {
            this.setState({ adultCounter: this.state.adultCounter - 1 });
        }
    }

    incrementChild = () => {
        if (this.state.adultCounter == 0) {
            this.setState({ adultCounter: this.state.adultCounter + 1 });
        }
        if (this.state.childCounter < 6) {
            this.setState({ childCounter: this.state.childCounter + 1 });
        }

    }

    decrementChild = () => {
        if (this.state.childCounter > 0) {
            this.setState({ childCounter: this.state.childCounter - 1 });
        }
    }

    incrementRoom = () => {
        if (this.state.roomCounter < 10) {
            this.setState({ roomCounter: this.state.roomCounter + 1 });
        }
    }

    decrementRoom = () => {
        if (this.state.roomCounter > 0) {
            this.setState({ roomCounter: this.state.roomCounter - 1 });
        }
    }
    componentDidMount() {
        MoviesData().then(
            (data) => {
                this.setState({

                    itemsZone: data,
                });
            },
            (error) => {
                this.setState({
                    error
                });
            }
        );
    }


    componentWillUnmount() {
        // document.removeEventListener('click', this.globalClickListener)
        window.removeEventListener('click', this.globalClickListener)
    }
    globalClickListener = (nativeEvent) => {
        
        this.setState({ dropdownVisible: false, showResults: false }, () => {
            console.log('global click')
            console.log(this.state.showResults)
            // document.removeEventListener('click', this.globalClickListener)
            window.removeEventListener('click', this.globalClickListener)
        })
    }
    toggleDropdown = (syntheticEvent) => {
        console.log('toggle dropdown')
        syntheticEvent.stopPropagation();
        this.setState(prevState => ({ dropdownVisible: !prevState.dropdownVisible }), () => {
            if (this.state.dropdownVisible) {
                console.log(this.state.dropdownVisible);

                // document.addEventListener('click', this.globalClickListener)
                window.addEventListener('click', this.globalClickListener)
            }
        })
        // console.log(this.state.dropdownVisible);
    }
    handleBodyClick = (event) => {
        console.log('body click')
        event.stopPropagation()
    }

    renderDropdownMenu() {
        return (
            <div className="panel-dropdown-content" onClick={this.handleBodyClick}>

                <div className="qtyButtons">

                    <label>Adults</label>
                    <button className="qtyDec1" onClick={this.decrement}>
                        &mdash;</button>
                    <span>{this.state.adultCounter}</span>
                    <button className="qtyInc1" onClick={this.increment}>
                        &#xff0b;</button>
                </div>
                <div className="qtyButtons">
                    <label>Childrens</label>
                    <button className="qtyDec1" onClick={this.decrementChild}>
                        &mdash;</button>
                    <span>{this.state.childCounter}</span>
                    <button className="qtyInc1" onClick={this.incrementChild}>
                        &#xff0b;</button>
                </div>
                <div className="qtyButtons">
                    <label>Rooms</label>
                    <button className="qtyDec1" onClick={this.decrementRoom}>
                        &mdash;</button>
                    <span>{this.state.roomCounter}</span>
                    <button className="qtyInc1" onClick={this.incrementRoom}>
                        &#xff0b;</button>
                </div>

            </div>
        )
    }



    getSuggestions = (value) => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;

        return inputLength === 0 ? [] : this.state.itemsZone.filter(lang =>
            lang.name.toLowerCase().slice(0, inputLength) === inputValue
        );
    };

    // When suggestion is clicked, Autosuggest needs to populate the input
    // based on the clicked suggestion. Teach Autosuggest how to calculate the
    // input value for every given suggestion.
    getSuggestionValue = (suggestion) => suggestion.name;

    // Use your imagination to render suggestions.
    renderSuggestion = (suggestion) => (
        <div className="font14 darkText  noChild  clickable"><div className="spaceBetween makeFlex hrtlCenter "><div className="flexOne"><p className="locusLabel appendBottom5">{suggestion.name}</p></div><span className="width60 font14 lightGreyText latoBold appendBottom5 appendLeft10 textRight" style={{width:'130px'}}>{suggestion.id}</span></div></div>
    );
    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue,
            defaultZoneval: newValue,
        });
    };

    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.
    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };

    // Autosuggest will call this function every time you need to clear suggestions.
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    onSuggestionSelected = () => {
        this.setState({
            showResults: false
        });
    }

    showZonelistdiv = (syntheticEvent) => {

        syntheticEvent.stopPropagation();
        this.setState(prevState => ({ showResults: !prevState.showResults }), () => {
            if (this.state.showResults) {
                console.log(this.state.showResults);

                // document.addEventListener('click', this.globalClickListener)
                window.addEventListener('click', this.globalClickListener)
            }
        })
    }

 


    render() {
        console.log(this.state.itemsZone);
        const menuClass = `panel-dropdown${this.state.dropdownVisible ? " active" : ""}`;
        const { value, suggestions } = this.state;

        // Autosuggest will pass through all these props to the input.
        const inputProps = {
            placeholder: 'Enter city/ Hotel/ Area/ Building',
            type: "search",
            value,
            onChange: this.onChange
        };

        return (

            <section className="hero_single version_2">

                <div className="wrapper" style={{backgroundColor:'white'}}>
                    <div className="container container-custom margin_30_95">
                        <div className="height" style={{ height: 100 }}></div>
                        <div className="Latin">

                            <BannerTabNav tabs={['Accommodations', 'Attractions & Things to Do', 'Packages', 'Land & Ferry Transfers', 'Liveaboard']} selected={this.state.selected} setSelectedTab={this.setSelectedTabOptions}>
                                <BannerTab isSelected={this.state.selected === 'Accommodations'}>
                                    <div className="row no-gutters custom-search-input-2" style={{ marginTop: '0px' }}>
                                        <div className="totalsercheng">
                                            <div className="col-lg-5">
                                                <div className="focused_div">
                                                    <div className="form-group India" onClick={this.showZonelistdiv}>
                                                        <span style={{width:'100%', textAlign:'initial'}}>City / Hotel / Area / Building</span>
                                                        <p className="defaultZone">{this.state.defaultZoneval}</p>
                                                        {this.state.showResults ?
                                                            <div className="autocomplete-wrapper" onClick={this.handleBodyClick}>
                                                                <Autosuggest
                                                                    suggestions={suggestions}
                                                                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                                                    onSuggestionSelected={this.onSuggestionSelected}
                                                                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                                                    getSuggestionValue={this.getSuggestionValue}
                                                                    renderSuggestion={this.renderSuggestion}
                                                                    inputProps={inputProps}
                                                                />
                                                            </div> : null
                                                        }
                                                    </div>
                                                </div>
                                            </div>


                                            <div className="col-lg-4" style={{ borderRight: '1px solid #c1c1c1' }}>
                                                <div className="form-group">
                                                    <span style={{ marginLeft: '-15px', marginBottom: '8px' }}>Check in - Check out</span>
                                                    <DateRangePicker
                                                        startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                                        startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                                                        endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                                        endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                                                        onDatesChange={({ startDate, endDate }) => this.setDatefunction(startDate, endDate)} // PropTypes.func.isRequired,
                                                        focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                                        onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                                                    />

                                                </div>
                                            </div>


                                            <div className="col-lg-2" >
                                                <div onClick={this.toggleDropdown}>

                                                    <div className={menuClass} >

                                                        <a href="javascript:;">Room & Guests </a>
                                                        {
                                                            this.state.dropdownVisible &&
                                                            this.renderDropdownMenu()
                                                        }


                                                        <div className="Guestss">
                                                            <p>{this.state.roomCounter}<span> Room</span>  {this.state.adultCounter + this.state.childCounter}<span> Guests</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="sercheng">
                                        <Link to={'/hotellisting'} className="btn_searchs">Search</Link>
                                    </div>
                                </BannerTab>


                                <BannerTab isSelected={this.state.selected === 'Attractions & Things to Do'}>
                                    <div className="row no-gutters custom-search-input-2" style={{ marginTop: '0px' }}>
                                        <div className="totalsercheng">
                                            <div className="col-lg-6">
                                                <div className="form-group India">
                                                    <span>Location</span>
                                                    <input className="form-control" type="text" placeholder="Where are you going?" />

                                                </div>
                                            </div>


                                            <div className="col-lg-6" style={{ borderRight: '1px solid #c1c1c1' }}>
                                                <div className="form-group">
                                                    <span style={{ marginLeft: '-15px', marginBottom: '8px', width: '100%', textAlign: 'justify' }}>Add when you want to go</span>
                                                    <div style={{ marginLeft: '35px', float: 'left' }}>
                                                        <DateRangePicker
                                                            startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                                            startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                                                            endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                                            endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                                                            onDatesChange={({ startDate, endDate }) => this.setDatefunction(startDate, endDate)} // PropTypes.func.isRequired,
                                                            focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                                            onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                                                        />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="sercheng">
                                        <button className="btn_searchs">Search</button>
                                    </div>
                                </BannerTab>
                                <BannerTab isSelected={this.state.selected === 'Packages'}>
                                    <div className="row no-gutters custom-search-input-2" style={{ marginTop: '0px' }}>
                                        <div className="totalsercheng">
                                            <div className="col-lg-6">
                                                <div className="form-group India">
                                                    <span>Location</span>
                                                    <input className="form-control" type="text" placeholder="Where are you going?" />

                                                </div>
                                            </div>


                                            <div className="col-lg-6" style={{ borderRight: '1px solid #c1c1c1' }}>
                                                <div className="form-group">
                                                    <span style={{ marginLeft: '-15px', marginBottom: '8px', width: '100%', textAlign: 'justify' }}>Add when you want to go</span>
                                                    <div style={{ marginLeft: '35px', float: 'left' }}>
                                                        <DateRangePicker
                                                            startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                                            startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                                                            endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                                            endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                                                            onDatesChange={({ startDate, endDate }) => this.setDatefunction(startDate, endDate)} // PropTypes.func.isRequired,
                                                            focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                                            onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                                                        />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="sercheng">
                                        <button className="btn_searchs">Search</button>
                                    </div>
                                </BannerTab>

                                <BannerTab isSelected={this.state.selected === 'Land & Ferry Transfers'}>
                                    <div className="row no-gutters custom-search-input-2" style={{ marginTop: '0px' }}>
                                        <div className="totalsercheng">
                                            <div className="col-lg-6">
                                                <div className="form-group India">
                                                    <span>Location</span>
                                                    <input className="form-control" type="text" placeholder="Where are you going?" />

                                                </div>
                                            </div>


                                            <div className="col-lg-6" style={{ borderRight: '1px solid #c1c1c1' }}>
                                                <div className="form-group">
                                                    <span style={{ marginLeft: '-15px', marginBottom: '8px', width: '100%', textAlign: 'justify' }}>Add when you want to go</span>
                                                    <div style={{ marginLeft: '35px', float: 'left' }}>
                                                        <DateRangePicker
                                                            startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                                            startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                                                            endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                                            endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                                                            onDatesChange={({ startDate, endDate }) => this.setDatefunction(startDate, endDate)} // PropTypes.func.isRequired,
                                                            focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                                            onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                                                        />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="sercheng">
                                        <button className="btn_searchs">Search</button>
                                    </div>
                                </BannerTab>

                                <BannerTab isSelected={this.state.selected === 'Liveaboard'}>
                                    <div className="row no-gutters custom-search-input-2" style={{ marginTop: '0px' }}>
                                        <div className="totalsercheng">
                                            <div className="col-lg-6">
                                                <div className="form-group India">
                                                    <span>Location</span>
                                                    <input className="form-control" type="text" placeholder="Where are you going?" />

                                                </div>
                                            </div>


                                            <div className="col-lg-6" style={{ borderRight: '1px solid #c1c1c1' }}>
                                                <div className="form-group">
                                                    <span style={{ marginLeft: '-15px', marginBottom: '8px', width: '100%', textAlign: 'justify' }}>Add when you want to go</span>
                                                    <div style={{ marginLeft: '35px', float: 'left' }}>
                                                        <DateRangePicker
                                                            startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                                            startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                                                            endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                                            endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                                                            onDatesChange={({ startDate, endDate }) => this.setDatefunction(startDate, endDate)} // PropTypes.func.isRequired,
                                                            focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                                            onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                                                        />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="sercheng">
                                        <button className="btn_searchs">Search</button>
                                    </div>
                                </BannerTab>

                            </BannerTabNav>
                        </div>

                        <div className="clearfix"></div>
  
                    </div>
                </div>

            </section>
        )
    }
}
export default Banner;
