import React from 'react'

export default function Footer() {
    return (
            <footer >
                <div style={{width:"100%"}} className="container margin_60_35">
                    <div className="row">
                        <div className="col-md-3"></div>
                        <div className="col-md-6">
                            <div id="newsletter">
                                <h6>Newsletter</h6>
                                <div id="message-newsletter"></div>
                                <form method="post" action="#" name="newsletter_form" id="newsletter_form">
                                    <div className="form-group">
                                        <input type="email" name="email_newsletter" id="email_newsletter" className="form-control" placeholder="Your email" />
                                        <input type="submit" value="Submit" id="submit-newsletter" />
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="col-md-3"></div>
                    </div>
                    <div className="footer-line"></div>
                    <div className="row">
                        <div className="col-lg-4 col-md-6">
                            <ul className="contacts">
                                <p><img src="images/logo/logo.png" alt="" className="logo_normal" /></p>
                                <li><a href="#"><i className="fa fa-map-marker" aria-hidden="true"></i> Location</a></li>
                                <li><a href="tel://2484376668"><i className="fa fa-phone" aria-hidden="true"></i> +248 437 6668</a></li>
                                <li><a href=" info@seychelles-holidays.travel"><i className="fa fa-envelope-o" aria-hidden="true"></i> info@seychelles-holidays.travel</a></li>
                            </ul>
                            <div className="follow_us">
                                <ul>
                                    <li>Follow us</li>
                                    <li><a href="#"><i className="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i className="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i className="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i className="fa fa-instagram" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6">
                            <h5>Holidays Seychelles</h5>
                            <ul className="links">
                                <li><a href="about-us.php">About Us</a></li>
                                <li><a href="#">Booking Terms & Conditions</a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Hep Center</a></li>
                            </ul>
                        </div>
                        <div className="col-lg-3 col-md-6">
                            <h5>Where To Go</h5>
                            <ul className="links">
                                <li><a href="#">Best Places To Stay</a></li>
                                <li><a href="#">Things To Do</a></li>
                                <li><a href="#">Best Deals & Packages</a></li>
                                <li><a href="#">Land Transfers</a></li>
                                <li><a href="#">Ferry Transfers</a></li>
                                <li><a href="#">Live Aboard</a></li>
                                <li><a href="#">Wedding</a></li>
                            </ul>
                        </div>
                        <div className="col-lg-2 col-md-6">
                            <h5>Visit Planner</h5>
                            <ul className="links">
                                <li><a href="#">Before your trip</a></li>
                                <li><a href="#">During Your Trip</a></li>
                                <li><a href="#">Visit Planner</a></li>
                                <li><a href="#">Family</a></li>
                                <li><a href="#">Romance</a></li>
                            </ul>
                        </div>
                    </div>
                
                        <div className="row">
                            <div className="col-lg-12">
                                <ul id="additional_links">
                                    <li><span>© 2019 Seychelles Holidays</span></li>
                                </ul>
                            </div>
                        </div>
                        </div>
                        </footer>
)
}
