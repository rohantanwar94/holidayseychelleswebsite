import React, { Component } from 'react';
import "./Navbar.css";


class Navbar extends Component
{
    constructor(props)
    {   
        super(props);

        this.state={
            classNameToggler:"toggle-nav"
        }
    }

    

    render()
    {
        window.onscroll=()=>{
            if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                this.setState({classNameToggler:"fixed-nav"});
              } else {
                this.setState({classNameToggler:"toggle-nav"});
              }
        }

        return (
            <div className={this.state.classNameToggler} >
                <ul className="nav">
                    <li className="nav-item"><a href="#" >Accomadations</a></li>
                    <li className="nav-item"><a href="#" >Attractions and Things to do</a></li>
                    <li className="nav-item"><a href="#" >Packages</a></li>
                    <li className="nav-item"><a href="#" >Land and ferry Transfers</a></li>
                    <li className="nav-item"><a href="#" >LiveAbroad</a></li>
                </ul>
                
            </div>
        )
    }
   
}

export default Navbar;


/*
<div className="right-links">
                <p><a href="#">My Trips<img src={icon} /></a></p>
                <p><a href="#">Offers</a></p>
                {/* <p><a href="#"><img src={icon2} /></a></p>} 
                // </div> 


                <div className="logo">
                <img src={Logo} />
                </div>
*/