// import React from 'react';
// class BannerTabNav extends React.Component {
//   render() {
//     return (
//       <div style={{ width: '100%' }}>
//         <ul className="nav nav-tabs">
//           {
//             this.props.tabs.map(tab => {
//               const active = (tab === this.props.selected ? 'active ' : '');
//               return (
//                 <li className="nav-item" key={tab}>
//                   <a className={"nav-link " + active} onClick={() => this.props.setSelectedTab(tab)}>
//                     {tab}
//                     <span className={"nav-span " + active}></span>
//                   </a>
//                 </li>
//               );
//             })
//           }
//         </ul>
//         { this.props.children}
//       </div>
//     );
//   }
// }
// export default BannerTabNav;


import React, { Component } from 'react';
import "./BannerTabNav.css";
import Icon1 from "../assets/images/background-images/icon01.png";
import Icon2 from "../assets/images/background-images/02.png";
import Icon3 from "../assets/images/background-images/icon04.png";
import Icon4 from "../assets/images/background-images/03.png";
import Icon5 from "../assets/images/background-images/06.png";
import Icon6 from "../assets/images/background-images/icon06.png";


// import "./Navbar.css";

// import Logo from "../assets/images/logo/logo-02.png";
// import icon from "../assets/images/favicon.ico";


class Navbar extends Component
{
    constructor(props)
    {   
        super(props);

        this.state={
            classNameToggler:"toggle-nav"
        }
    }

    

    render()
    {
        // window.onscroll=()=>{
        //     if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        //         this.setState({classNameToggler:"fixed-nav"});
        //       } else {
        //         this.setState({classNameToggler:"toggle-nav"});
        //       }
        // }

        return (
            <div className="fixed-nav1" >
                {/* <div className="logo">
                <img src={Logo} />
                </div> */}
                <ul className="nav">
                    <li className="nav-item"><a href="#" ><img src={Icon1} /><span>Accomadations</span></a></li>
                    <li className="nav-item"><a href="#" ><img src={Icon2} /><span>Attractions and Things to do</span></a></li>
                    <li className="nav-item"><a href="#" ><img src={Icon3} /><span>Land and ferry Transfers</span></a></li>
                    <li className="nav-item"><a href="#" ><img src={Icon4} /><span>Deals and Packages</span></a></li>
                    <li className="nav-item"><a href="#" ><img src={Icon5} /><span>Wedding Packages</span></a></li>
                    <li className="nav-item"><a href="#" ><img src={Icon6} /><span>Live Abroad</span></a></li>
                </ul>
                { this.props.children}
            </div>
        )
    }
   
}

export default Navbar;



{/* <div className="right-links">
                <p><a href="#">My Trips<img src={icon} /></a></p>
                <p><a href="#">Offers</a></p>
                {/* <p><a href="#"><img src={icon2} /></a></p> */}
              // </div> */}